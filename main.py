flag = True
counter = 100
bouncyTotal = 0

def isBouncy(x):
    number = str(x)
    increasingDigits = 0
    decreasingDigits = 0
    previous = int(number[0])
    for ch in number[1:]:
        if previous < int(ch):
            increasingDigits += 1
        if previous > int(ch):
            decreasingDigits += 1

        previous = int(ch)
        if increasingDigits > 0 and decreasingDigits > 0:
            return True
    return False


# Enter the target proportion you want to reach
targetProportion = 0.99

while flag:
    if isBouncy(counter): bouncyTotal += 1
    if (float(bouncyTotal)/float(counter) >= targetProportion):
        flag = False
    else:
        counter += 1

print ('The least bouncy number for which the proportion reaches {0:.0f}% is {1}.'.format(targetProportion*100, counter))
